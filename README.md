<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Azure CDN Static Website hosting with Gandi DNS

Terraform module which create Azure CDN that can host a static website and use Gandi DNS for custom domain registration.

## Prerequisites

- An Azure Active Directory user that have a Global administrator role
- An active Gandi provider API key. You can retrieve your API key by visiting the [Account Management screen](https://account.gandi.net/en/), going to the `Security` tab and generating your Production API Key

## Module usage

```hcl
provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = false
    }
  }
}

provider "gandi" {
}

module "azure_static_website_with_gandi_dns" {
  source = "../../"

  location                      = var.location
  app_name                      = var.app_name
  resource_group_name           = var.resource_group_name
  domain                        = var.domain
  subdomain                     = var.subdomain
  alternatives_domains          = var.alternatives_domains
  cdn_location                  = var.cdn_location
  cdn_public_html_path          = "${path.cwd}/${var.cdn_public_html_path}"
  cdn_reroute_all_rules         = var.cdn_reroute_all_rules
  cdn_enable_compression        = var.cdn_enable_compression
  cdn_content_types_to_compress = var.cdn_content_types_to_compress
  tags                          = var.tags
}

```
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | = 2.92.0 |
| <a name="requirement_gandi"></a> [gandi](#requirement\_gandi) | 2.0.0-rc3 |

## Resources

| Name | Type |
|------|------|
| [azurerm_cdn_endpoint.endpoint](https://registry.terraform.io/providers/hashicorp/azurerm/2.92.0/docs/resources/cdn_endpoint) | resource |
| [azurerm_cdn_profile.profile](https://registry.terraform.io/providers/hashicorp/azurerm/2.92.0/docs/resources/cdn_profile) | resource |
| [azurerm_storage_account.storage_account](https://registry.terraform.io/providers/hashicorp/azurerm/2.92.0/docs/resources/storage_account) | resource |
| [azurerm_storage_account_network_rules.storage_account_network_rules](https://registry.terraform.io/providers/hashicorp/azurerm/2.92.0/docs/resources/storage_account_network_rules) | resource |
| [gandi_livedns_record.record](https://registry.terraform.io/providers/psychopenguin/gandi/2.0.0-rc3/docs/resources/livedns_record) | resource |
| [null_resource.add_custom_domain](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.blob_upload](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [null_resource.dns_record_check](https://registry.terraform.io/providers/hashicorp/null/latest/docs/resources/resource) | resource |
| [random_string.suffix](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/string) | resource |
| [azurerm_resource_group.rg](https://registry.terraform.io/providers/hashicorp/azurerm/2.92.0/docs/data-sources/resource_group) | data source |
| [http_http.myip](https://registry.terraform.io/providers/hashicorp/http/latest/docs/data-sources/http) | data source |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 2.84.0 |
| <a name="provider_gandi"></a> [gandi](#provider\_gandi) | 2.0.0-rc3 |
| <a name="provider_http"></a> [http](#provider\_http) | 2.1.0 |
| <a name="provider_null"></a> [null](#provider\_null) | 3.1.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alternatives_domains"></a> [alternatives\_domains](#input\_alternatives\_domains) | List of others subdomains which the certificate will match | `list(any)` | `[]` | no |
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | The name of the hosted app for resource naming purpose | `string` | n/a | yes |
| <a name="input_cdn_content_types_to_compress"></a> [cdn\_content\_types\_to\_compress](#input\_cdn\_content\_types\_to\_compress) | List of content types to compress | `list(string)` | <pre>[<br>  "text/html",<br>  "text/css",<br>  "text/plain",<br>  "text/xml",<br>  "text/x-component",<br>  "text/javascript",<br>  "application/x-javascript",<br>  "application/javascript",<br>  "application/json",<br>  "application/manifest+json",<br>  "application/vnd.api+json",<br>  "application/xml",<br>  "application/xhtml+xml",<br>  "application/rss+xml",<br>  "application/atom+xml",<br>  "application/vnd.ms-fontobject",<br>  "application/x-font-ttf",<br>  "application/x-font-opentype",<br>  "application/x-font-truetype",<br>  "image/svg+xml",<br>  "image/x-icon",<br>  "image/vnd.microsoft.icon",<br>  "font/ttf",<br>  "font/eot",<br>  "font/otf",<br>  "font/opentype"<br>]</pre> | no |
| <a name="input_cdn_enable_compression"></a> [cdn\_enable\_compression](#input\_cdn\_enable\_compression) | Sets whether to enable compression in order to reduce bandwidth usage | `bool` | `true` | no |
| <a name="input_cdn_location"></a> [cdn\_location](#input\_cdn\_location) | Azure region where the CDN will be created | `string` | `"westeurope"` | no |
| <a name="input_cdn_public_html_path"></a> [cdn\_public\_html\_path](#input\_cdn\_public\_html\_path) | Static web hosting root directory without the trailing slash | `string` | n/a | yes |
| <a name="input_cdn_reroute_all_rules"></a> [cdn\_reroute\_all\_rules](#input\_cdn\_reroute\_all\_rules) | Additional rewrite rules for CDN endpoint, usually used with react apps | `any` | `{}` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | domain for the CDN custom endpoint | `string` | `"contoso.com"` | no |
| <a name="input_index_document"></a> [index\_document](#input\_index\_document) | The name of the document (html) file to be used as index | `string` | `"index.html"` | no |
| <a name="input_location"></a> [location](#input\_location) | Azure region where the resource group is located | `string` | `"france central"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Ressource group where the ressources will be created | `string` | n/a | yes |
| <a name="input_storage_account_settings"></a> [storage\_account\_settings](#input\_storage\_account\_settings) | Map of settings for the storage account | `map(string)` | <pre>{<br>  "account_kind": "StorageV2",<br>  "account_replication_type": "LRS",<br>  "account_tier": "Standard"<br>}</pre> | no |
| <a name="input_subdomain"></a> [subdomain](#input\_subdomain) | Sub domain for the CDN custom endpoint | `string` | `"blog"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags for resources | `map(string)` | <pre>{<br>  "environment": "test"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_customdomainname"></a> [customdomainname](#output\_customdomainname) | List of URL(s) binded to CDN |
| <a name="output_static_website_cdn_endpoint_url"></a> [static\_website\_cdn\_endpoint\_url](#output\_static\_website\_cdn\_endpoint\_url) | CDN endpoint URL for Static website |
| <a name="output_static_website_url"></a> [static\_website\_url](#output\_static\_website\_url) | static web site URL from storage account |  
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
